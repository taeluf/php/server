# Status

## Versions
- v0.1: for liaison v0.5
- v0.2: for liaison v0.6
- v0.3: probably will be lia v0.6, but MIGHT be a lia v0.7. Will include many QoL improvements. 

## v0.3 (hopes/dreams)
These are things i WANT for the next version. Some of these things will require changes in dependent libraries

Setting up a new site sucks. Integrating all the packages sucks. Having to fiddle with code when things could be handled by config files sucks. Not having a GUI for any of it sucks. I want to make some significant changes to this package & liaison & phad & whatever to make everything easier. So here is kind of what I want:
- 3 or 4 lines of code to setup a server. (autoload, instantiate Server, initialize, deliver)
- self-contained apps. No longer cluttering `init/server.php` with a bunch of setup code
- config-driven server: app dirs, backup dir, upload dir, log dir, ...
- built-in user login / registration, including phad integration
- hooks on views, before & after their content
- hooks on routes, before & after their content
- `R()` integrated
- built-in server/deployment/other tools: push to staging server. push to production server. setup env files on server. setup an ssh key on server. upload local database to remote db. backup database. cron support (for backups at the very least!).
- built-in setup tools: deliver script + scaffold (composer, tests, phad views, env, lia app, backup dir, orm items, lilmigrations)
    - composer.json: include Server's dependencies directly in the scaffolded composer.json
- utilities: logging, email sending, make CREATE TABLE statements from phad items... idk
- niceties: breadcrumbs, share buttons, bottom bar for edit buttons, allow a route to cancel itself (thus deferring to the next route or giving a 404), spam controls for forms  ...
- lilorm/lildb/lilmigrations: Including phad integration for orm objects
- maybe eventually: liaison class generator (would have all methods that are dynamically added). a theme folder NOT nested within the view folder. namespaced views. access-settings on routes.
- debug view/page: show all views, phad items, all methods, all routes, sitemaps, forms, configs ...etc?

## V0.2 notes, apr 28, 2022
I've updated to lia v0.6 & made some changes to the server class. All my tests are passing, and i think everything is good to go! Though i know this library is a little shy on tests, so i'll need to better test my webservers before pushing to production with this new update

## Notes
- must have `.phptest-host` file in current working directory or cwd()./test/Server.
- must have dir `cache` in `$_SERVER['DOCUMENT_ROOT']`


## March 31, 2022
Writing a very simple implementation of some very simple analytics
I need to write tests for both the database initialization & the putting of analytics

Done! It's been added! Well ... the storage of a url & an md5 hash of an ip address ... there's no views or anything yet

## About
This is a starter website setup to use from the vendor dir. No site layout or themes. Just the basic delivery stuff to make Liaison and phad work

## TODO
- test
    - DONE (for phad only) sitemap creation (add a new phad file)
    - DONE error page generation
- add simple cli?? (or other simple interface)
    - DONE to generate the error page
    - DONE recompile all phad views
    - MEH copy sample theme files
    - MEH copy setup files (deliver.php, a config file, idk what else)
    - delete liaison cache files
- polish
    - setup code scrawl
    - type & document everything
    - clean up code
- idk
    - MEH taeluf/lildb
- code stuff
    - MEH zero-config setup, config-file for easy configuration
    - LATER optional caching of md-to-html conversions
    - LATER phad user-stuff ... idk what to do with that
- idk2
    - DONE (taeluf.com) transition an existing site to use this new server lib
- LATER Test Class for easy testing (some may go in phptest??)
    - test production server
    - test home page & other individual pages
    - test compiled css file
    - test seo content
    - test markdown to html conversion

## pure html error page
- public path to a generic error page
    - uses the built-in theme
- cli requests that page, gets the url of the compiled css file
- modifies the output html to print the css file directly in the html
- stores the modified html to the error page path

## Done
- DONE?? use the 'deliver' wrapper function
- DONE?? setup sample tests
- DONE composer require taeluf/php-env (after adding it to packagist)
- DONE make git repo, add to packagist
- DONE actually write the markdown filter
