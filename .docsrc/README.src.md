# Taeluf's Server | Liaison & Phad
Easy-to-setup server using my foss libs. 

**For an example setup see @see_file(test/Server/).**

## Notice March 24, 2022 !!!
user-ship is not ready yet, so many features of Phad are not valuable. You CAN set-up user hooks yourself, but meh.

## Dependencies
- @easy_link(tlf, php/liaison): The server framework
- @easy_link(tlf, php/lia/phad): A library that compiles pure-html (with some custom tags) into database-connected views. The html may contain php for additional customization.

### Extra Stuff
- @easy_link(tlf, php/env): for dealing with different environments (localhost/production)
- @easy_link(tlf, php/cli): simple cli interface
- @easy_link(tlf, php/code-scrawl): Generate this documentation
- @easy_link(tlf, php/lexer): Generat ASTs (used by Code Scrawl)
- @easy_link(tlf, php/better-regex): Regex stuff ... but I don't think code scrawl uses it any more ... 
- @easy_link(tlf, php/tester): Testing Library

## Notice
I write this for myself. I also write tests & document relatively well. This means it's probably safe for you to use. 

However, since I'm currently focused on solving my own needs, I may deprecate or cause breaking changes without warning. Any BIG breaking changes will likely be a new branch with a version bump. But no promises.

## Install
@template(php/composer_install, taeluf/server)

## Basic Setup
1. create a deliver.php script (see below)
2. Create some of the folder structure (better to see @see_file(test/Server/)) for a full example.
3. run `vendor/bin/tlfserv` to generate an error page, sitemap (only from phad items), and to re-compile phad items
4. View documentation for liaison & phad (see above) to figure out how to do everything. Good luck!

## Cli Stuff
For additional info, see @see_file(bin/tlfserv)
Execute `vendor/bin/tlfserv` to run all commands, or run them individually:
- `tlfserv error-page`: generate an error page
- `tlfserv generate-sitemap`: generate a sitemap for PHAD items only (does not sitemap any non-phad routes. Sorry)
- `tlfserv recompile-phad`: Recompile all PHAD items.

## File/Folder structure
- deliver.php: The script that runs your whole server.
- public: files that are routed by liason automatically
- phad: contains views for phad
- view: contains simple views for liaison
- file: files for direct download (uses a faster router and skips liaison)
- fastroute: scripts for direct execution (uses a faster router and skips liaison)
- cache: cache dir
- sitemap: where the sitemap file goes

## Sample deliver.php script
```php
@file(test/Server/deliver.php)
```

## Classes

@ast(class.Tlf\Server, ast/class)

@ast(class.Tlf\Server\Helper, ast/class)
