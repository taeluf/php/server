<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Server.php  
  
# class Tlf\Server  
A (somewhat) minimal layer on top of Liaison & Phad to make them much easier to setup.  
  
  
## Constants  
  
## Properties  
- `static public $IS_TLFSERV_CLI = false;` set TRUE to stop deliver() from delivering  
- `public bool $debug = false;` if true, print error messages & always recompile everything  
See init() for initialization.  
- `public bool $enable_error_route = true;` Set false before calling init() if you want to implement your own /generic-error-page/ route  
- `public array $phads = [];` array of phad objects  
- `public \Env $env;` environment object  
- `public \Lia $lia;` Liaison object  
- `public \Phad $phad;` The most recently set Phad object. See $this->phads for list of all phad instances  
- `public \Lia\Package $main;` The main server package  
  
## Methods   
- `public function __construct()`   
- `public function init($debug=null)` Initialize liaison.   
- `public function load_env(string $file)` Load environment settings file  
- `public function set_cache_dir(string $dir)`   
- `public function disable_theme()` To disable the theme which generally provides a full HTML doc  
- `public function file_route($dir)` quickly route to a file in $dir  
- `public function php_route($dir)` quickly route to a script in $dir  
- `public function is_request_to(string $url_prefix)` Check if the request uri starts with the given string  
- `public function addServer($dir,$name=null, $url_prefix'/')` Add a standard Liaison server package for the given dir  
- `public function env_pdo()` Get a pdo object with a mysql connection that's defined in your env settings file  
The settings must define:  
mysql.host, mysql.dbname, mysql.user, & mysql.password  
- `public function phad_item(string $item, array $args = [])`   
- `public function enable_phad(string $dir, \PDO $pdo, $phad_class='Phad', $route_prefix'')`   
- `public function deliver()` Run `$lia->deliver()` & automatically handle errors  
- `public function debug()`   
- `public function enable_analytics(\PDO $pdo)` 1. Save the current page to analytics   
2. Enable the analytics route, if phad is enabled  
  
