<?php

namespace Tlf\Server;

class Helper {

    /**
     * Convert markdown to html, if CommonMark is installed
     *
     * @param $markdown
     * @return html, or if CommonMark is not installed, returns the markdown
     */
    public function markdown_to_html(string $markdown){
        
        $class = "League\\CommonMark\\CommonMarkConverter";
        if (class_exists($class,true)){
            $converter = new \League\CommonMark\CommonMarkConverter();
            return $converter->convert($markdown).'';
        }
        return $markdown;
    }
}
