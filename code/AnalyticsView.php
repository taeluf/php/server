<route pattern="/analytics/"></route>  


<h1>Analytics across our site</h1>
<p>Analytics are helpful to learn what web pages are being visited. This analytics library attempts to be as privacy-respecting as possible, though the technical implementation is pretty lazy.</p>

<h2>For all time</h2>
<p>Well, since this analytics lib was installed anyway.</p>
<table border="1" style="border-collapse:collapse;">  
    <thead><th>Url</th><th>Hits</th></thead>
        <tbody>
        <tr item="tlf_analytics" > 
            <p-data sql="SELECT url, COUNT(*) as hits FROM tlf_analytics GROUP BY url ORDER BY hits DESC LIMIT 300"></p-data>
            <td prop="url"></td>
            <td prop="hits"></td>
        </tr>
        </tbody>
</table>

<h2>This Month</h2>
<?php 
$datetime = new \DateTime();
$year_month = $datetime->format("Y-m")."-01"; 
$args['year_month'] = $year_month;
?>
<p>Showing analytics for <?=$year_month?> (we store all visits as being on the first day of the month)</p>

<table border="1" style="border-collapse:collapse;">  
    <thead><th>Url</th><th>Hits</th></thead>
        <tbody>
        <tr item="tlf_analytics" >
            <p-data sql="SELECT url, COUNT(*) as hits FROM tlf_analytics WHERE `year_month` LIKE :year_month GROUP BY url ORDER BY hits DESC LIMIT 300"></p-data>
            <td prop="url"></td>
            <td prop="hits"></td>
        </tr>
        </tbody>
</table>


<h2>Privacy</h2>
<p>As of Apr 7, 2022, we no longer store any IP address or hash of IP address. We only record the page visited & the year+month it was visited. There is no personal info stored.</p>
