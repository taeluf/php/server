<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Taeluf's Server | Liaison & Phad  
Easy-to-setup server using my foss libs.   
  
**For an example setup see [test/Server/](/test/Server/).**  
  
## Notice March 24, 2022 !!!  
user-ship is not ready yet, so many features of Phad are not valuable. You CAN set-up user hooks yourself, but meh.  
  
## Dependencies  
- [php/liaison](https://tluf.me/php/liaison): The server framework  
- [php/lia/phad](https://tluf.me/php/lia/phad): A library that compiles pure-html (with some custom tags) into database-connected views. The html may contain php for additional customization.  
  
### Extra Stuff  
- [php/env](https://tluf.me/php/env): for dealing with different environments (localhost/production)  
- [php/cli](https://tluf.me/php/cli): simple cli interface  
- [php/code-scrawl](https://tluf.me/php/code-scrawl): Generate this documentation  
- [php/lexer](https://tluf.me/php/lexer): Generat ASTs (used by Code Scrawl)  
- [php/better-regex](https://tluf.me/php/better-regex): Regex stuff ... but I don't think code scrawl uses it any more ...   
- [php/tester](https://tluf.me/php/tester): Testing Library  
  
## Notice  
I write this for myself. I also write tests & document relatively well. This means it's probably safe for you to use.   
  
However, since I'm currently focused on solving my own needs, I may deprecate or cause breaking changes without warning. Any BIG breaking changes will likely be a new branch with a version bump. But no promises.  
  
## Install  
```bash  
composer require taeluf/server v0.2.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/server": "v0.2.x-dev"}}  
```  
  
  
## Basic Setup  
1. create a deliver.php script (see below)  
2. Create some of the folder structure (better to see [test/Server/](/test/Server/)) for a full example.  
3. run `vendor/bin/tlfserv` to generate an error page, sitemap (only from phad items), and to re-compile phad items  
4. View documentation for liaison & phad (see above) to figure out how to do everything. Good luck!  
  
## Cli Stuff  
For additional info, see [bin/tlfserv](/bin/tlfserv)  
Execute `vendor/bin/tlfserv` to run all commands, or run them individually:  
- `tlfserv error-page`: generate an error page  
- `tlfserv generate-sitemap`: generate a sitemap for PHAD items only (does not sitemap any non-phad routes. Sorry)  
- `tlfserv recompile-phad`: Recompile all PHAD items.  
  
## File/Folder structure  
- deliver.php: The script that runs your whole server.  
- public: files that are routed by liason automatically  
- phad: contains views for phad  
- view: contains simple views for liaison  
- file: files for direct download (uses a faster router and skips liaison)  
- fastroute: scripts for direct execution (uses a faster router and skips liaison)  
- cache: cache dir  
- sitemap: where the sitemap file goes  
  
## Sample deliver.php script  
```php  
<?php  
  
require(dirname(__DIR__,2).'/vendor/autoload.php');  
  
$_SERVER['HTTP_HOST'] = $_SERVER['HTTP_HOST'] ?? 'localhost';  
  
$debug = true;  
$dir = __DIR__;  
set_error_handler(  
    function($errno, $errstr, $errfile, $errline) {  
        throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);  
    }  
);  
  
$pdo = new \PDO('sqlite:'.$dir.'/db.sqlite');  
  
$server = new \Tlf\Server();  
$server->file_route($dir.'/file/');  
$server->php_route($dir.'/fastroute/');  
  
$server->init($debug);  
  
$server->enable_phad($dir, $pdo);  
  
$server->enable_analytics($pdo);  
  
  
  
// i think $lia is used by the cli  
$lia = $server->lia;  
$site = $server->addServer($dir);  
  
$server->deliver();  
```  
  
## Classes  
  
# class Tlf\Server  
A (somewhat) minimal layer on top of Liaison & Phad to make them much easier to setup.  
  
  
## Constants  
  
## Properties  
- `static public $IS_TLFSERV_CLI = false;` set TRUE to stop deliver() from delivering  
- `public bool $debug = false;` if true, print error messages & always recompile everything  
See init() for initialization.  
- `public bool $enable_error_route = true;` Set false before calling init() if you want to implement your own /generic-error-page/ route  
- `public array $phads = [];` array of phad objects  
- `public \Env $env;` environment object  
- `public \Lia $lia;` Liaison object  
- `public \Phad $phad;` The most recently set Phad object. See $this->phads for list of all phad instances  
- `public \Lia\Package $main;` The main server package  
  
## Methods   
- `public function __construct()`   
- `public function init($debug=null)` Initialize liaison.   
- `public function load_env(string $file)` Load environment settings file  
- `public function set_cache_dir(string $dir)`   
- `public function disable_theme()` To disable the theme which generally provides a full HTML doc  
- `public function file_route($dir)` quickly route to a file in $dir  
- `public function php_route($dir)` quickly route to a script in $dir  
- `public function is_request_to(string $url_prefix)` Check if the request uri starts with the given string  
- `public function addServer($dir,$name=null, $url_prefix'/')` Add a standard Liaison server package for the given dir  
- `public function env_pdo()` Get a pdo object with a mysql connection that's defined in your env settings file  
The settings must define:  
mysql.host, mysql.dbname, mysql.user, & mysql.password  
- `public function phad_item(string $item, array $args = [])`   
- `public function enable_phad(string $dir, \PDO $pdo, $phad_class='Phad', $route_prefix'')`   
- `public function deliver()` Run `$lia->deliver()` & automatically handle errors  
- `public function debug()`   
- `public function enable_analytics(\PDO $pdo)` 1. Save the current page to analytics   
2. Enable the analytics route, if phad is enabled  
  
  
  
# class Tlf\Server\Helper  
  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function markdown_to_html(string $markdown)` Convert markdown to html, if CommonMark is installed  
  
  
  
