<?php

require(dirname(__DIR__,2).'/vendor/autoload.php');

$_SERVER['HTTP_HOST'] = $_SERVER['HTTP_HOST'] ?? 'localhost';

$debug = true;
$dir = __DIR__;
set_error_handler(
    function($errno, $errstr, $errfile, $errline) {
        throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
    }
);

$pdo = new \PDO('sqlite:'.$dir.'/db.sqlite');

$server = new \Tlf\Server();
$server->file_route($dir.'/file/');
$server->php_route($dir.'/fastroute/');

$server->init($debug);

$server->enable_phad($dir, $pdo);

$server->enable_analytics($pdo);



// i think $lia is used by the cli
$lia = $server->lia;
$site = $server->addServer($dir);

$server->deliver();
