<?php

namespace Tlf\Server\Test;

class Main extends \Tlf\Tester {

    public function testAnalytics(){
        $response = $this->get('/analytics/');

        echo $response;
        $this->str_contains($response, '/analytics/', '<table ');
    }

    public function testPhad(){
        $response = $this->get('/phad/');
        echo $response;
        $this->str_contains($response, '<h1>Main</h1>');
    }

    public function testView(){
        $response = $this->get('/view/');
        echo $response;
        $this->str_contains($response, '<h1>Sample View</h1>');

    }

    public function testMain(){
        $response = $this->get('/');
        echo $response;
        $this->str_contains($response, 'index page');
    }

    public function testFastPhp(){
        $response = $this->get('/something/');
        echo $response;
        $this->compare('something', $response);
    }

    public function testFastFile(){
        $response = $this->get('/something.txt');
        echo $response;
        $this->compare('something.txt', $response);
    }
}

