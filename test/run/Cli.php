<?php

namespace Tlf\Server\Test;

class Cli extends \Tlf\Tester {

    public function testAll(){

        // remove compiled files
        $server = $this->file('test/Server/');
        $main=$server.'/phad/main.compiled.php';
        $sitemap=$server.'/phad/with-sitemap.compiled.php';
        if (file_exists($main))unlink($main);
        if (file_exists($sitemap))unlink($sitemap);

        
        // remove sitemap file
        $file = $this->file('test/Server/sitemap/sitemap.xml');
        if (file_exists($file)){
            unlink($file);
        }


        // remove error page
        $error_file = $this->file('/test/Server/cache/generic-error-page.html');
        if (file_exists($error_file)){
            unlink($error_file);
        }

        system('cd "'.$server.'"; ../../bin/tlfserv');

        $this->test('recompiling');
        $this->file_exists($main);
        $this->file_exists($sitemap);


        $this->test("sitemap generation");
        $this->str_contains(
            file_get_contents($file),
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">',
            '<loc>/phad-sitemap/</loc>',
        );


        $this->test('generate error file');
        $error_html = file_get_contents($error_file);
        $this->str_contains($error_html,
            '<!DOCTYPE html>',
            'color:#12AF34;',
            '<meta name="robots" content="noindex" />',
        );
    }

    public function testRecompilePhadItems(){

        $server = $this->file('test/Server/');
        $main=$server.'/phad/main.compiled.php';
        $sitemap=$server.'/phad/with-sitemap.compiled.php';
        if (file_exists($main))unlink($main);
        if (file_exists($sitemap))unlink($sitemap);

        system('cd "'.$server.'"; ../../bin/tlfserv recompile-phad');

        $this->file_exists($main);
        $this->file_exists($sitemap);
    }

    public function testMakeSitemap(){
        $file = $this->file('test/Server/sitemap/sitemap.xml');
        if (file_exists($file)){
            unlink($file);
        }
        $server = $this->file('test/Server/');
        system('cd "'.$server.'"; ../../bin/tlfserv generate-sitemap');

        $this->str_contains(
            file_get_contents($file),
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">',
            '<loc>/phad-sitemap/</loc>',
        );
    }

    public function testMakeErrorPage(){
        $file = $this->file('test/Server/cache/generic-error-page.html');
        if (file_exists($file)){
            unlink($file);
        }
        
        $server = $this->file('test/Server/');
        system('cd "'.$server.'"; ../../bin/tlfserv error-page');

        $error_html = file_get_contents($file);
        $this->str_contains($error_html,
            '<!DOCTYPE html>',
            'color:#12AF34;',
            '<meta name="robots" content="noindex" />',
        );
    }

}

