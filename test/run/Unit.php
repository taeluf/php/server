<?php

namespace Tlf\Server\Test;

class Unit extends \Tlf\Tester {

    public function testMineMd5IPs(){
        echo "This is a script to generate a list of IP addresses & md5 hash them to see what kind of computational power would be required to reverse-engineer an md5-ip analytics database. As of Apr 5, 2022, my analytics md5 hash the ip for db storage. I believe I will be removing this in the near future.";
        $this->disable();
        return;
        $fh = fopen($this->file('test/output/ip-list.txt'), 'w');
        for ($i=0;$i<1000000;$i++){
            $ip = rand(0,255).'.'.rand(0,255).'.'.rand(0,255).'.'.rand(0,255)."\n";
            fwrite($fh,$ip);
        }
        fclose($fh);

        $inh = fopen($this->file('test/output/ip-list.txt'), 'r');
        $outh = fopen($this->file('test/output/md5-list.txt'), 'w');
        while ($line=fgets($inh)){
            fwrite($outh, trim($line).': '. md5($line)."\n");
        }

        fclose($inh);
        fclose($outh);
    }

    public function testMarkdownFilter(){

        $helper = new \Tlf\Server\Helper();

        $html = $helper->markdown_to_html('# Header');

        $this->compare(
            '<h1>Header</h1>',
            $html
        );
    }
}

